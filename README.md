# Vanillia Minecraft

Vanillia is a Minecraft modpack designed to enhance the player's experience by adding numerous client-side features to the game. These features are intended to make the game more enjoyable while maintaining performance for most hardware configurations. Vanillia offers VR and Controller support and allows players to join any Vanilla Minecraft server, including older versions of the game. Although it does not add any new gameplay mechanics, Vanillia provides a smoother and more immersive Minecraft experience that is sure to be enjoyable to all players.

## Disclaimer

I am not responsible for server bans, please check with server admins if you have any concerns. ESPECIALLY on servers that are on older versions of Minecraft than 1.19

## Notable Mods in this pack

- [BetterF3](https://modrinth.com/mod/betterf3)
  - Enhances the F3 debug screen with new information and features
- [Bobby](https://modrinth.com/mod/bobby)
  - Caches chunks and allows rendering them beyond server render distance
- [Essential](https://modrinth.com/mod/essential)
  - Provides a suite of multiplayer-focused features
- [Controlify](https://modrinth.com/mod/controlify)
  - Adds controller support to Minecraft
  - Things like HOTAS, Steering Wheels, and more are supported
- [Replay Mod](https://modrinth.com/mod/replay)
  - Allows recording and playback of gameplay sessions
- [ViaFabric](https://modrinth.com/mod/viafabric)
  - Allows connecting to servers running older versions of Minecraft
- [Vivecraft](https://modrinth.com/mod/vivecraft)
  - Adds support for virtual reality (VR) headsets to Minecraft
- [Simple Voice Chat](https://modrinth.com/mod/simple-voice-chat)
  - Adds proximity voice chat to Minecraft
  - Server must have the [Simple Voice Chat Plugin](https://www.spigotmc.org/resources/simple-voice-chat.93738/) installed

### Minimap Mods

- [Xaero's Minimap](https://modrinth.com/mod/xaeros-minimap) & [World Map](https://modrinth.com/mod/xaeros-world-map)
  - Client Side minimap and world map that looks similar to minecraft's theme

### Server Side Mods

- [Bottled Air](https://modrinth.com/mod/bottled-air)
  - Allows using empty bottles underwater to get air
  - Client enabled
- [Carpet](https://modrinth.com/mod/carpet)
  - Adds a variety of features to servers
- [CobbleGen](https://modrinth.com/mod/cobblegen)
  - Allows customizing Cobblestone Generators based off y-level
  - Client enabled
- [CodeServer](https://modrinth.com/mod/code-server)
  - Allows running a VSCode server on a Minecraft server
	- [coder/code-server](https://github.com/coder/code-server)
  - *Linux Servers Only*
  - Requires another port to be open for the VSCode server
- [infix](https://modrinth.com/mod/infix)
  - Makes mending and infinity work together
  - Client enabled
- [LuckPerms](https://modrinth.com/mod/luckperms)
  - Adds a permissions system to servers
- [Polysit](https://modrinth.com/mod/polysit)
  - Allows sitting on stairs and slabs
  - Client enabled
- [Styled Chat](https://modrinth.com/mod/styled-chat)
  - Allows customizing the chat
  - Integrates with LuckPerms
- [Styled Nicknames](https://modrinth.com/mod/styled-nicknames)
  - Allows customizing player names
  - Integrates with LuckPerms
- [Styled Player List](https://modrinth.com/mod/styled-player-list)
  - Allows customizing the player list
  - Integrates with LuckPerms
- [Universal Shops](https://modrinth.com/mod/universal-shops)
  - Allows players to create shops on servers

# Non-Modrinth Mods

The Following mods are not on Modrinth, but are still included in the pack
- [Controlling](https://www.curseforge.com/minecraft/mc-mods/controlling)
  - Allows viewing and changing keybinds in-game with ease
	- Dependant: [Searchables](https://www.curseforge.com/minecraft/mc-mods/searchables)
- [ViaRewind](https://www.curseforge.com/minecraft/mc-mods/viarewind)
- [ViaBackwards](https://www.curseforge.com/minecraft/mc-mods/viabackwards)
- [Yosbr](https://www.curseforge.com/minecraft/mc-mods/yosbr)
  - Allows me to ship default configs
  - Modrinth release is not updated to latest yet (for some reason)
	  